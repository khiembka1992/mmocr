_base_ = [
    '../../_base_/default_runtime.py',
    '../../_base_/recog_models/robust_scanner.py'
]

log_config = dict(
    interval=100,
    hooks=[
        dict(type='TextLoggerHook'),
        dict(type='WandbLoggerHook')
    ])

dict_file = 'dataset_all/vietnamese_character_dict.txt'
label_convertor = dict(
    type='AttnConvertor', dict_file=dict_file, with_unknown=False, lower=False, max_seq_len=30)

hybrid_decoder = dict(type='SequenceAttentionDecoder')

position_decoder = dict(type='PositionAttentionDecoder')

model = dict(
    type='RobustScanner',
    backbone=dict(type='ResNet31OCR'),
    encoder=dict(
        type='ChannelReductionEncoder',
        in_channels=512,
        out_channels=128,
    ),
    decoder=dict(
        type='RobustScannerDecoder',
        dim_input=512,
        dim_model=128,
        hybrid_decoder=hybrid_decoder,
        position_decoder=position_decoder),
    loss=dict(type='SARLoss'),
    label_convertor=label_convertor,
    max_seq_len=30)

# optimizer
optimizer = dict(type='Adam', lr=1e-3)
optimizer_config = dict(grad_clip=None)
# learning policy
lr_config = dict(policy='step', step=[3, 4])
total_epochs = 200

img_norm_cfg = dict(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
train_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(
        type='ResizeOCR',
        height=48,
        min_width=48,
        max_width=160,
        keep_aspect_ratio=True,
        width_downsample_ratio=0.25),
    dict(type='ToTensorOCR'),
    dict(type='NormalizeOCR', **img_norm_cfg),
    dict(
        type='Collect',
        keys=['img'],
        meta_keys=[
            'filename', 'ori_shape', 'resize_shape', 'text', 'valid_ratio'
        ]),
]
test_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(
        type='MultiRotateAugOCR',
        rotate_degrees=[0, 90, 270],
        transforms=[
            dict(
                type='ResizeOCR',
                height=48,
                min_width=48,
                max_width=160,
                keep_aspect_ratio=True,
                width_downsample_ratio=0.25),
            dict(type='ToTensorOCR'),
            dict(type='NormalizeOCR', **img_norm_cfg),
            dict(
                type='Collect',
                keys=['img'],
                meta_keys=[
                    'filename', 'ori_shape', 'resize_shape', 'valid_ratio'
                ]),
        ])
]

dataset_type = 'OCRDataset'

train_prefix = 'dataset_all/'

train_img_prefix1 = train_prefix + '1/train'
train_img_prefix2 = train_prefix + '2/train'
train_img_prefix3 = train_prefix + '3/train'
train_img_prefix4 = train_prefix + '4/train'
train_img_prefix5 = train_prefix + '5/train'
train_img_prefix6 = train_prefix + '6/train'
train_img_prefix7 = train_prefix + '7/train'
train_img_prefix8 = train_prefix + '8/train'
train_img_prefix9 = train_prefix + '9/train'
train_img_prefix10 = train_prefix + '10/train'
train_img_prefix11 = train_prefix + '11/train'
train_img_prefix12 = train_prefix + '12/train'

train_ann_file1 = train_prefix + '1/label_train.txt'
train_ann_file2 = train_prefix + '2/label_train.txt'
train_ann_file3 = train_prefix + '3/label_train.txt'
train_ann_file4 = train_prefix + '4/label_train.txt'
train_ann_file5 = train_prefix + '5/label_train.txt'
train_ann_file6 = train_prefix + '6/label_train.txt'
train_ann_file7 = train_prefix + '7/label_train.txt'
train_ann_file8 = train_prefix + '8/label_train.txt'
train_ann_file9 = train_prefix + '9/label_train.txt'
train_ann_file10 = train_prefix + '10/label_train.txt'
train_ann_file11 = train_prefix + '11/label_train.txt'
train_ann_file12 = train_prefix + '12/label_train.txt'

train1 = dict(
    type=dataset_type,
    img_prefix=train_img_prefix1,
    ann_file=train_ann_file1,
    loader=dict(
        type='HardDiskLoader',
        repeat=20,
        parser=dict(
            type='LineStrParser',
            keys=['filename', 'text'],
            keys_idx=[0, 1],
            separator=' ')),
    pipeline=None,
    test_mode=False)

train2 = {key: value for key, value in train1.items()}
train2['img_prefix'] = train_img_prefix2
train2['ann_file'] = train_ann_file2

train3 = {key: value for key, value in train1.items()}
train3['img_prefix'] = train_img_prefix3
train3['ann_file'] = train_ann_file3

train4 = {key: value for key, value in train1.items()}
train4['img_prefix'] = train_img_prefix4
train4['ann_file'] = train_ann_file4

train5 = {key: value for key, value in train1.items()}
train5['img_prefix'] = train_img_prefix5
train5['ann_file'] = train_ann_file5

train6 = {key: value for key, value in train1.items()}
train6['img_prefix'] = train_img_prefix6
train6['ann_file'] = train_ann_file6

train7 = {key: value for key, value in train1.items()}
train7['img_prefix'] = train_img_prefix7
train7['ann_file'] = train_ann_file7

train8 = {key: value for key, value in train1.items()}
train8['img_prefix'] = train_img_prefix8
train8['ann_file'] = train_ann_file8

train9 = {key: value for key, value in train1.items()}
train9['img_prefix'] = train_img_prefix9
train9['ann_file'] = train_ann_file9

train10 = {key: value for key, value in train1.items()}
train10['img_prefix'] = train_img_prefix10
train10['ann_file'] = train_ann_file10

train11 = {key: value for key, value in train1.items()}
train11['img_prefix'] = train_img_prefix11
train11['ann_file'] = train_ann_file11

train12 = {key: value for key, value in train1.items()}
train12['img_prefix'] = train_img_prefix12
train12['ann_file'] = train_ann_file12

test_prefix = 'dataset_all/'
test_img_prefix1 = test_prefix + '1/test'
test_img_prefix2 = test_prefix + '2/test'
test_img_prefix3 = test_prefix + '3/test'
test_img_prefix4 = test_prefix + '4/test'
test_img_prefix5 = test_prefix + '5/test'
test_img_prefix6 = test_prefix + '6/test'
test_img_prefix7 = test_prefix + '7/test'
test_img_prefix8 = test_prefix + '8/test'
test_img_prefix9 = test_prefix + '9/test'
test_img_prefix10 = test_prefix + '10/test'
test_img_prefix11 = test_prefix + '11/test'
test_img_prefix12 = test_prefix + '12/test'

test_ann_file1 = test_prefix + '1/label_test.txt'
test_ann_file2 = test_prefix + '2/label_test.txt'
test_ann_file3 = test_prefix + '3/label_test.txt'
test_ann_file4 = test_prefix + '4/label_test.txt'
test_ann_file5 = test_prefix + '5/label_test.txt'
test_ann_file6 = test_prefix + '6/label_test.txt'
test_ann_file7 = test_prefix + '7/label_test.txt'
test_ann_file8 = test_prefix + '8/label_test.txt'
test_ann_file9 = test_prefix + '9/label_test.txt'
test_ann_file10 = test_prefix + '10/label_test.txt'
test_ann_file11 = test_prefix + '11/label_test.txt'
test_ann_file12 = test_prefix + '12/label_test.txt'

test1 = dict(
    type=dataset_type,
    img_prefix=test_img_prefix1,
    ann_file=test_ann_file1,
    loader=dict(
        type='HardDiskLoader',
        repeat=1,
        parser=dict(
            type='LineStrParser',
            keys=['filename', 'text'],
            keys_idx=[0, 1],
            separator=' ')),
    pipeline=None,
    test_mode=True)

test2 = {key: value for key, value in test1.items()}
test2['img_prefix'] = test_img_prefix2
test2['ann_file'] = test_ann_file2

test3 = {key: value for key, value in test1.items()}
test3['img_prefix'] = test_img_prefix3
test3['ann_file'] = test_ann_file3

test4 = {key: value for key, value in test1.items()}
test4['img_prefix'] = test_img_prefix4
test4['ann_file'] = test_ann_file4

test5 = {key: value for key, value in test1.items()}
test5['img_prefix'] = test_img_prefix5
test5['ann_file'] = test_ann_file5

test6 = {key: value for key, value in test1.items()}
test6['img_prefix'] = test_img_prefix6
test6['ann_file'] = test_ann_file6

test7 = {key: value for key, value in test1.items()}
test7['img_prefix'] = test_img_prefix7
test7['ann_file'] = test_ann_file7

test8 = {key: value for key, value in test1.items()}
test8['img_prefix'] = test_img_prefix8
test8['ann_file'] = test_ann_file8

test9 = {key: value for key, value in test1.items()}
test9['img_prefix'] = test_img_prefix9
test9['ann_file'] = test_ann_file9

test10 = {key: value for key, value in test1.items()}
test10['img_prefix'] = test_img_prefix10
test10['ann_file'] = test_ann_file10

test11 = {key: value for key, value in test1.items()}
test11['img_prefix'] = test_img_prefix11
test11['ann_file'] = test_ann_file11

test12 = {key: value for key, value in test1.items()}
test12['img_prefix'] = test_img_prefix12
test12['ann_file'] = test_ann_file12

data = dict(
    samples_per_gpu=256,
    workers_per_gpu=2,
    val_dataloader=dict(samples_per_gpu=1),
    test_dataloader=dict(samples_per_gpu=1),
    train=dict(
        type='UniformConcatDataset',
        datasets=[
            train1, train2, train3, train4, train5, train6, train7, train8, train9, train10, train11, train12
        ],
        pipeline=train_pipeline),
    val=dict(
        type='UniformConcatDataset',
        datasets=[test1, test2, test3, test4, test5, test6, test7, test8, test9, test10, test11, test12],
        pipeline=test_pipeline),
    test=dict(
        type='UniformConcatDataset',
        datasets=[test1, test2, test3, test4, test5, test6, test7, test8, test9, test10, test11, test12],
        pipeline=test_pipeline))

evaluation = dict(interval=1, metric='acc')
